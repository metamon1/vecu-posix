/**
 * AS - the open source Automotive Software on https://github.com/parai
 *
 * Copyright (C) 2017 AS <parai@foxmail.com>
 *
 * This source code is free software; you can redistribute it and/or modify it
 * under the terms of the GNU General Public License version 2 as published by the
 * Free Software Foundation; See <http://www.gnu.org/licenses/old-licenses/gpl-2.0.txt>.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY
 * or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License
 * for more details.
 */
/* ============================ [ INCLUDES  ] ====================================================== */
#include "Rte_Telltale.h"
/* ============================ [ MACROS    ] ====================================================== */
/* ============================ [ TYPES     ] ====================================================== */
/* ============================ [ DECLARES  ] ====================================================== */
/* ============================ [ DATAS     ] ====================================================== */
/* ============================ [ LOCALS    ] ====================================================== */
/* ============================ [ FUNCTIONS ] ====================================================== */
#include <stdio.h>
#include <signal.h>
#include <unistd.h>

#include "Lcd.h"

//#include "/home/okdongja/autosar/autoas/aaaa.h"
//#include "/home/okdongja/autosar/autoas/com/as.infrastructure/arch/posix/mcal/Lcd1.h"

#ifdef USE_posix	
#define ON true
#define OFF false
#define bool boolean

extern int a;
extern bool AirbagState;
extern bool AutoCruiseState;
extern bool HighBeamState;
extern bool LowOilState;
extern bool PosLampState;
extern bool SeatbeltDriverState;
extern bool SeatbeltPassengerState;
extern bool TPMSState;
extern bool TurnLeftState;
extern bool TurnRightState;
#endif

#include <unistd.h>

/* void (*old_fun)(int);

void sigint_handler (int signo)
{
	printf("%d", signo);
	if (a == 0) {
		a = 1;
	} else if (a == 1) {
		a = 0;
	}
	
	printf( "\n a값 변경 : %d\n", a);
	
	signal(SIGINT, SIG_DFL); //또는 signal( SIGINT, SIG_DFL);
}  */

//#include <gtk/gtk.h>

void Telltale_run(void)
{	// period is 20ms
	//printf("%d", a);

/*     old_fun = signal( SIGINT, sigint_handler);
 */
#ifdef USE_posix	
	if(AirbagState == ON) {
		Rte_Write_Telltale_AirbagState(RTE_CONST_OnOff_On);
	} else if (AirbagState == OFF) {
		Rte_Write_Telltale_AirbagState(RTE_CONST_OnOff_Off);
	}


	if(AutoCruiseState == ON) {
		Rte_Write_Telltale_AutoCruiseState(RTE_CONST_OnOff_On);
	} else if (AutoCruiseState == OFF) {
		Rte_Write_Telltale_AutoCruiseState(RTE_CONST_OnOff_Off);
	}


	if(HighBeamState == ON) {
		Rte_Write_Telltale_HighBeamState(RTE_CONST_OnOff_On);
	} else if (HighBeamState == OFF) {
		Rte_Write_Telltale_HighBeamState(RTE_CONST_OnOff_Off);
	}


	if(LowOilState == ON) {
		Rte_Write_Telltale_LowOilState(RTE_CONST_OnOff_On);
	} else if (LowOilState == OFF) {
		Rte_Write_Telltale_LowOilState(RTE_CONST_OnOff_Off);
	}


	if(PosLampState == ON) {
		Rte_Write_Telltale_PosLampState(RTE_CONST_OnOff_On);
	} else if (PosLampState == OFF) {
		Rte_Write_Telltale_PosLampState(RTE_CONST_OnOff_Off);
	}


	if(SeatbeltDriverState == ON) {
		Rte_Write_Telltale_SeatbeltDriverState(RTE_CONST_OnOff_On);
	} else if (SeatbeltDriverState == OFF) {
		Rte_Write_Telltale_SeatbeltDriverState(RTE_CONST_OnOff_Off);
	}


	if(SeatbeltPassengerState == ON) {
		Rte_Write_Telltale_SeatbeltPassengerState(RTE_CONST_OnOff_On);
	} else if (SeatbeltPassengerState == OFF) {
		Rte_Write_Telltale_SeatbeltPassengerState(RTE_CONST_OnOff_Off);
	}


	if(TPMSState == ON) {
		Rte_Write_Telltale_TPMSState(RTE_CONST_OnOff_On);
	} else if (TPMSState == OFF) {
		Rte_Write_Telltale_TPMSState(RTE_CONST_OnOff_Off);
	}


	if(TurnLeftState == ON) {
		Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_1Hz);
		Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_Off);

	} else if (TurnLeftState == OFF) {
		Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_Off);
	}


	if(TurnRightState == ON) {
		Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_1Hz);
		Rte_Write_Telltale_TurnLeftState(RTE_CONST_OnOff_Off);

	} else if (TurnRightState == OFF) {
		Rte_Write_Telltale_TurnRightState(RTE_CONST_OnOff_Off);
	}
#endif
	//Rte_Write_Telltale_QQQQQQQQQQQQQQQQQQQState(RTE_CONST_OnOff_Off);

#ifdef USE_versatilepb
	Rte_Write_Telltale_AirbagState(OnOff_On);
	Rte_Write_Telltale_AutoCruiseState(OnOff_1Hz);
	Rte_Write_Telltale_HighBeamState(OnOff_3Hz);
	Rte_Write_Telltale_LowOilState(OnOff_1Hz);
	Rte_Write_Telltale_PosLampState(OnOff_2Hz);
	Rte_Write_Telltale_SeatbeltDriverState(OnOff_3Hz);
	Rte_Write_Telltale_SeatbeltPassengerState(OnOff_1Hz);
	Rte_Write_Telltale_TPMSState(OnOff_2Hz);
	Rte_Write_Telltale_TurnLeftState(OnOff_3Hz);
	Rte_Write_Telltale_TurnRightState(OnOff_3Hz);
#endif

}











