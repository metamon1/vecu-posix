/* #ifdef USE_ON_OFF_BUTTON

#include <gtk/gtk.h>
#include <gdk/gdk.h>

#include "Button.h"
#include "Switch.h"

#include <stdio.h>
#include <assert.h>
#include <sys/time.h>
#include <pthread.h>

#include "lvgl/lvgl.h"

#include "asdebug.h"

#include "Lcd.h"
extern int AsWsjOnline(void);

static void*            lcdThread2   = NULL;





static void lcd_main_quit2(void)
{
  lcdThread2 = NULL;

  gtk_main_quit();
}

 void *Button_Init(void *param)
{
  GtkWidget *window2;

#ifdef __WINDOWS__
  Sleep(1);
#else
  usleep(1700);
#endif

#ifdef USE_SG
  if (AsWsjOnline())
  {
    lcdThread2 = NULL;
    return 0;
  }
#endif

  printf("# Lcd22222222222222_Thread Enter\n");

  gtk_init(NULL, NULL);


  window2 = gtk_window_new(GTK_WINDOW_TOPLEVEL);
  gtk_window_set_title(GTK_WINDOW(window2), "Button");

  GtkWidget *button_box = gtk_box_new(GTK_ORIENTATION_VERTICAL, 0);
  gtk_container_add(GTK_CONTAINER(window2), button_box);
	
  GtkWidget *toggle1;
  GtkWidget *toggle2;
  GtkWidget *toggle3;
  GtkWidget *toggle4;
  GtkWidget *toggle5;
  GtkWidget *toggle6;
  GtkWidget *toggle7;
  GtkWidget *toggle8;
  GtkWidget *toggle9;
  GtkWidget *toggle10;

  toggle1 = gtk_toggle_button_new_with_label("Airbag_Switch");
  gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(toggle1), TRUE);
  g_signal_connect(toggle1, "toggled", G_CALLBACK(Airbag_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle1);

  toggle2 = gtk_toggle_button_new_with_label("AutoCruise_Switch");
  gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(toggle2), TRUE);
  g_signal_connect(toggle2, "toggled", G_CALLBACK(AutoCruise_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle2);

  toggle3 = gtk_toggle_button_new_with_label("HighBeam_Switch");
  gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(toggle3), TRUE);
  g_signal_connect(toggle3, "toggled", G_CALLBACK(HighBeam_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle3);

  toggle4 = gtk_toggle_button_new_with_label("LowOil_Switch");
  gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(toggle4), TRUE);
  g_signal_connect(toggle4, "toggled", G_CALLBACK(LowOil_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle4);

  toggle5 = gtk_toggle_button_new_with_label("PosLamp_Switch");
  gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(toggle5), TRUE);
  g_signal_connect(toggle5, "toggled", G_CALLBACK(PosLamp_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle5);

  toggle6 = gtk_toggle_button_new_with_label("SeatbeltDriver_Switch");
  gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(toggle6), TRUE);
  g_signal_connect(toggle6, "toggled", G_CALLBACK(SeatbeltDriver_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle6);

  toggle7 = gtk_toggle_button_new_with_label("SeatbeltPassenger_Switch");
  gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(toggle7), TRUE);
  g_signal_connect(toggle7, "toggled", G_CALLBACK(SeatbeltPassenger_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle7);

  toggle8 = gtk_toggle_button_new_with_label("TPMS_Switch");
  gtk_toggle_button_set_mode(GTK_TOGGLE_BUTTON(toggle8), TRUE);
  g_signal_connect(toggle8, "toggled", G_CALLBACK(TPMS_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle8);

  toggle9 = gtk_button_new_with_label("TurnLeft_Switch");
  g_signal_connect(toggle9, "clicked", G_CALLBACK(TurnLeft_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle9);

  toggle10 = gtk_button_new_with_label("TurnRight_Switch");
  g_signal_connect(toggle10, "clicked", G_CALLBACK(TurnRight_button), NULL);
  gtk_container_add(GTK_CONTAINER(button_box), toggle10);

  //////////////////////////////
  //////////////////////////////
  gtk_widget_show_all(window2);

  g_signal_connect(window2, "destroy", G_CALLBACK(lcd_main_quit2), NULL);

  gtk_main();

  printf("# Lcd222222222_Thread Exit\n");

  exit(0);


  return 0;

  
}



void Lcd_Init2(void)
{
    
    pthread_create( (pthread_t*)&lcdThread2, NULL, Button_Init, (void *)NULL);


}
#ifdef LCD_TEST
int main(int argc, char *argv[])
{
  Lcd_Init2();
  while (1)
    ;
  return 0;
}
#endif

#endif
 */