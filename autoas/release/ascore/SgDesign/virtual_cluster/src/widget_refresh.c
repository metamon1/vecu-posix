#ifdef USE_SG
#include "Sg.h"
#include "Stmo.h"
#include "Dio.h"

#define TT_ON  1
#define TT_OFF SGL_INVALID


	
extern int AsWsjOnline(void);

/**
 * @brief 클러스터 화면에서 Tacho Pointer의 각도를 표시 하는 함수
 * 
 */
void CacheClusterTachoPointer(SgWidget* w){}
void* RefreshClusterTachoPointer(SgWidget* w)
{
	Stmo_DegreeType degree;
	
	Stmo_GetPosDegree(STMO_ID_TACHO,&degree);

	if(!AsWsjOnline())
	{
		w->d = (degree/STMO_ONE_DEGREE)%360;
	}
	else
	{
		w->d = degree;
	}

	return 0;
}

/**
 * @brief 클러스터 화면에서 Speed Pointer의 각도를 표시 하는 함수
 * 
 */
void CacheClusterSpeedPointer(SgWidget* w){}
void* RefreshClusterSpeedPointer(SgWidget* w)
{
	Stmo_DegreeType degree;

	Stmo_GetPosDegree(STMO_ID_SPEED,&degree);

	if(!AsWsjOnline())
	{
		w->d = (degree/STMO_ONE_DEGREE)%360;
	}
	else
	{
		w->d = degree;
	}

	return 0;
}

/**
 * @brief 클러스터 내부 TPMS, LowOil, PosLamp, TurnLeft, TurnRight, AutoCruise, HighBeam, SeatbeltDriver, SeatbeltPassenger, Airbag의 on off를 표시 하는 함수
 * 
 */
void CacheTelltaleTPMS(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleTPMS))
	{																									
		w->l = TT_ON;										
	}														
	else													
	{															
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleTPMS(SgWidget* w) { return 0; }	


void CacheTelltaleLowOil(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleLowOil))
	{																											
		w->l = TT_ON;										
	}														
	else													
	{														
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleLowOil(SgWidget* w) { return 0; }	


void CacheTelltalePosLamp(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltalePosLamp))
	{																											
		w->l = TT_ON;										
	}														
	else													
	{														
		w->l = TT_OFF;										
	}
}
void* RefreshTelltalePosLamp(SgWidget* w) { return 0; }	


void CacheTelltaleTurnLeft(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleTurnLeft))
	{																											
		w->l = TT_ON;										
	}														
	else													
	{														
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleTurnLeft(SgWidget* w) { return 0; }	




void CacheTelltaleTurnRight(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleTurnRight))
	{																											
		w->l = TT_ON;										
	}														
	else													
	{														
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleTurnRight(SgWidget* w) { return 0; }	


void CacheTelltaleAutoCruise(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleAutoCruise))
	{																											
		w->l = TT_ON;										
	}														
	else													
	{														
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleAutoCruise(SgWidget* w) { return 0;} 	



void CacheTelltaleHighBeam(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleHighBeam))
	{													
																
		w->l = TT_ON;										
	}														
	else													
	{											
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleHighBeam(SgWidget* w) { return 0; }	


void CacheTelltaleSeatbeltDriver(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleSeatbeltDriver))
	{																											
		w->l = TT_ON;										
	}														
	else													
	{														
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleSeatbeltDriver(SgWidget* w) { return 0; }	


void CacheTelltaleSeatbeltPassenger(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleSeatbeltPassenger))
	{																											
		w->l = TT_ON;										
	}														
	else													
	{														
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleSeatbeltPassenger(SgWidget* w) { return 0; }	


void CacheTelltaleAirbag(SgWidget* w) {
	
	if(STD_HIGH == Dio_ReadChannel(DIO_CHL_TelltaleAirbag))
	{																											
		w->l = TT_ON;										
	}														
	else													
	{														
		w->l = TT_OFF;										
	}
}
void* RefreshTelltaleAirbag(SgWidget* w) { return 0; }

/**
 * @brief 클러스터 화면에서 Temp Pointer의 각도를 표시 하는 함수
 * 
 */
void CacheClusterTempPointer(SgWidget* w){}
void* RefreshClusterTempPointer(SgWidget* w)
{
	Stmo_DegreeType degree;

	Stmo_GetPosDegree(STMO_ID_TEMP,&degree);

	if(!AsWsjOnline())
	{
		w->d = (degree/STMO_ONE_DEGREE)%360;
	}
	else
	{
		w->d = degree;
	}

	return 0;
}

/**
 * @brief 클러스터 화면에서 Fuel Pointer의 각도를 표시 하는 함수
 * 
 */
void CacheClusterFuelPointer(SgWidget* w){}
void* RefreshClusterFuelPointer(SgWidget* w)
{
	Stmo_DegreeType degree;

	Stmo_GetPosDegree(STMO_ID_FUEL,&degree);

	if(!AsWsjOnline())
	{
		w->d = (degree/STMO_ONE_DEGREE)%360;
	}
	else
	{
		w->d = degree;
	}
	
	return 0;
}


#endif /* widget_refresh.c */
